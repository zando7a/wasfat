import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_connection_checker/data_connection_checker.dart';

class InternetHelper {
  final DataConnectionChecker _checker;

  InternetHelper(this._checker);
  Future<bool> get isConnected async => await _checker.hasConnection;

  Future<GetOptions> internetValidation() async {
    GetOptions options;
    if (await isConnected)
      options = GetOptions(source: Source.serverAndCache);
    else
      options = GetOptions(source: Source.cache);
    return options;
  }
}
