import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wasfat_akl/helper/internet_helper.dart';
import 'package:wasfat_akl/models/dish.dart';
import 'package:wasfat_akl/models/food_category.dart';

class FoodCategoryProvider extends ChangeNotifier {
  final FirebaseFirestore _firestore;
  final InternetHelper _helper;

  FoodCategoryProvider(this._firestore, this._helper);
  var foodCategories = Map<String, FoodCategory>();
  var topCategories = <FoodCategory>[];
  var dishesRecentlyAdded = <Dish>[];
  final listKey = GlobalKey<SliverAnimatedListState>();

  Future<void> getDishesByCategory(String categoryId) async {
    final query = await _firestore
        .collection('dishes')
        .where('categoryId', isEqualTo: categoryId)
        .get(await _helper.internetValidation());
    final categoryDishes = query.docs
        .map<Dish>((dish) => Dish.fromMap(dish.data()))
        .toList()
          ..shuffle();
    foodCategories.update(categoryId,
        (foodCategory) => foodCategory.copyWith(dishes: categoryDishes));

    notifyListeners();
  }

  Future<void> getFoodCategory() async {
    final query = await _firestore
        .collection('food_category')
        .orderBy('priority')
        .get(await _helper.internetValidation());
    if (query.docs.isNotEmpty)
      foodCategories.addEntries(query.docs.map((foodCategory) =>
          MapEntry<String, FoodCategory>(foodCategory.data()['id'],
              FoodCategory.fromMap(foodCategory.data()))));

    topCategories = query.docs
        .map((foodCategory) => FoodCategory.fromMap(foodCategory.data()))
        .toList()
          ..shuffle();
    notifyListeners();
  }

  Future<void> getDishesRecentlyAdded() async {
    final query = await _firestore
        .collection('dishes')
        .orderBy('addDate', descending: true)
        .limit(5)
        .get(await _helper.internetValidation());
    dishesRecentlyAdded =
        query.docs.map<Dish>((dish) => Dish.fromMap(dish.data())).toList();
    notifyListeners();
  }
}
